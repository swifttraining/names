//
//  Name.swift
//  Names
//
//  Created by Romaine Hinds on 10/4/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class Name {
    
    let id: UUID
    var firstName: String
    var lastName: String
    var age: Int
    
    init(firstName: String, lastName: String, age: Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
        id = UUID()
    }

}
