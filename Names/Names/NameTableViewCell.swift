//
//  NameTableViewCell.swift
//  Names
//
//  Created by Romaine Hinds on 10/4/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class NameTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
