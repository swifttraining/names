//
//  NamesViewController.swift
//  Names
//
//  Created by Romaine Hinds on 10/4/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class NamesViewController: UIViewController {

    var names: [Name]!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let name1 = Name(firstName: "Bob", lastName: "IT", age: 22)
        let name2 = Name(firstName: "Yakeem", lastName: "Noel", age: 25)
        
        names = [name1, name2]
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}


extension NamesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! NameTableViewCell
        let name = names[indexPath.row]
        cell.nameLabel.text = "\(name.firstName) \(name.lastName)"
        return cell
        
        
    }
}
